import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class BlocTest extends StatelessWidget {
  build(context) {
    return MaterialApp(
          home: BlocProvider(
        child: MyApp(),
        create: (context) => MyBloc(),
      ),
    );
  }
}

enum CEvent { cre, su }

class MyBloc extends Bloc<CEvent, int> {
  @override
  int get initialState => 0;

  @override
  Stream<int> mapEventToState(CEvent event) async* {
    switch (event) {
      case CEvent.cre:
        yield state + 1;
        break;
      case CEvent.su:
        yield state - 1;
    }
  }
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
 MyBloc blocProvider;
@override
  void dispose() {
    blocProvider?.close();
    super.dispose();
  }

  @override
  Widget build(context) {
    blocProvider = BlocProvider.of<MyBloc>(context);
    return Scaffold(
        body: Center(
          child: BlocBuilder<MyBloc,int>(
            builder: (context, count) {
              return Text('$count');
            },
          ),
        ),
        floatingActionButton: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  blocProvider.add(CEvent.cre);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: FloatingActionButton(
                child: Icon(Icons.remove),
                onPressed: () {
                  blocProvider.add(CEvent.su);
                },
              ),
            ),
          ],
        ),
    );
  }
}
