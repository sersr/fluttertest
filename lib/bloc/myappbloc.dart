import 'package:bloc/bloc.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

enum ShowOrHide { scrolling, no }

class MyAppBloc extends Bloc<ShowOrHide, bool> {
  @override
  bool get initialState => false;

  @override
  Stream<bool> mapEventToState(ShowOrHide event) async* {
    switch (event) {
      case ShowOrHide.scrolling:
        yield true;
        break;
      case ShowOrHide.no:
        yield false;
    }
  }
}

class ItemsBloc extends Bloc<ReEvent, List<int>> {
  @override
  List<int> get initialState => _items;
  List<int> _items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  final duration = Duration(seconds: 1);
  @override
  Stream<List<int>> mapEventToState(ReEvent event) async* {
    switch (event) {
      // case ReEvent.init:
      //   yield _items;
      //   break;
      case ReEvent.refresh:
        // _refresh();\
        if (_items.length > 100) {
          final items = _items;
          _items = List<int>.generate(14, (v) => items[v]);
        }
        final caches = _genrateCaches();
        _items.insertAll(0, caches);
        await Future.delayed(duration);
        yield [..._items];
        break;
      case ReEvent.loading:
        // _loading();
        if (_items.length == double.infinity) {
          final items = _items;
          _items = List<int>.generate(14, (v) => items[v]);
        }
        final caches = _genrateCaches();
        _items..addAll(caches);
        await Future.delayed(duration);
        yield [..._items];
        break;
    }
  }

  // void _refresh() {
  //   final caches = _genrateCaches();
  //   _items.insertAll(0, caches);
  // }

  // void _loading() {

  // }

  List<int> _genrateCaches() {
    final _length = _items.length;

    final caches = List<int>.generate(10, (v) => v + _length);
    return caches;
  }
}

enum ReEvent { refresh, loading }

// enum ReEvent { add, no }

// class MyReState extends Bloc<ReEvent, Repos> {
//   @override
//   Repos get initialState => NoRepos();

//   @override
//   Stream<Repos> mapEventToState(ReEvent event) async* {
//     switch (event) {
//       case ReEvent.add:
//         yield MyRepository();
//         break;
//       case ReEvent.no:
//         yield NoRepos();
//     }
//   }
// }

// abstract class Repos{

// }

// class NoRepos extends Repos{}

// class MyRepository extends Repos{
//   MyRepository() {
//     _items = List<Home>.generate(
//         10,
//         (v) => Home(
//               index: v,
//               key: ValueKey<int>(v),
//             ));
//   }
//   List<Home> _items;
//   List<Home> get items => _items;
//   List<Home> _itemCaches = [];
//   void _data() {
//     final itemlength = _items.length;
//     _itemCaches = List<Home>.generate(
//         10,
//         (v) => Home(
//               index: v + itemlength,
//               key: ValueKey<int>(v + itemlength),
//             ));
//   }

//   void onLoading() async {
//     // monitor network fetch
//     _data();
//     _items.addAll(_itemCaches);
//     // await Future.delayed(Duration(milliseconds: 1000));
//     // if failed,use loadFailed(),if no data return,use LoadNodata()
//     // _refreshController.loadComplete();
//   }

//   void onRefresh() async {
//     // monitor network fetch
//     if (_items.length > 100) _items = _items.getRange(0, 14);
//     _data();
//     _items.insertAll(0, _itemCaches);
//     // await Future.delayed(Duration(milliseconds: 1000));
//     // if (mounted) setState(() {});
//     // // if failed,use refreshFailed()
//     // _refreshController.refreshCompleted();
//   }
// }
