import 'package:demowin/src/bilibili.dart';
import 'package:demowin/src/multilayout.dart';
import 'package:demowin/src/pagebuilder.dart';
import 'package:demowin/src/renderbox.dart';
import 'package:flutter/material.dart';

import 'slidbale/aslidable.dart';
import 'src/custumscroll.dart';
import 'src/scrollable.dart';
import 'src/second.dart';
import 'src/spalsh.dart';
import 'src/sticky.dart';
import 'src/test.dart';
import 'src/third.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: theme,
      initialRoute: "/bilibili",
      routes: {
        "/": (context) => MainPage(),
        "/second": (context) => Second(),
        "/slidable": (context) => Myslidable(),
        "/third": (context) => HeroTest(),
        "/pagebuilder": (context) => PageBuilderTest(),
        "/adpage": (context) => AdPage(),
        "/bilibili": (context) => BbHome(),
        "/test": (context) => Tsfw(),
        "/sticky": (context) => Sticky(),
        "/cust": (context) => Cust(),
        "/mw": (context) => Mw(),
        "/layout":(context) => Mylayout(),
        "/scroll": (context) => Pinned(),
      },
      onGenerateRoute: _onGenerateRoute,
      onUnknownRoute: _onUnknownRoute,
      onGenerateTitle: (context)=>'戏',
      title: '戏',
    );
  }

  // theme
  final theme = ThemeData(
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {TargetPlatform.android: MyPageTransition()},
    ),
  );

  // GenerateRoute
  Route<dynamic> _onGenerateRoute(RouteSettings setting) {
    if (setting.name == '/ad') {
      final _hoffsetTween = Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset.zero);
      final _seoffsetTween = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(0.2, 0.0));

      return PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryanimation) {
          return AdPage();
        },
        transitionsBuilder:
            (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
          final sec = SlideTransition(
            child: child,
            position: CurvedAnimation(
              parent: secondaryAnimation,
              curve: Curves.easeInOut,
            ).drive(_seoffsetTween),
          );

          final slidetransition = SlideTransition(
            child: sec,
            position: CurvedAnimation(
              parent: animation,
              curve: Curves.easeInOut,
            ).drive(_hoffsetTween),
          );

          return slidetransition;
        },
      );
    }
    return null;
  }

  // UnknownRoute
  Route<dynamic> _onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        body: Center(
          child: FlatButton(
            child: Text('The routeSetting is${settings.name}'),
            onPressed: () {
              Navigator.maybePop(context);
            },
          ),
        ),
      );
    });
  }
}

