import 'package:flutter/material.dart';

class HeroTest extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.teal,
          child: Hero(
            tag: 'icon',
            child: Icon(
              Icons.ac_unit,
              size: 200,
            ),
          ),
        ),
      ),
    );
  }
}
