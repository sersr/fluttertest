import 'package:flutter/material.dart';

class Cust extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CustState();
  }
}

class _CustState extends State<Cust> with SingleTickerProviderStateMixin {
  TabController tabController;
  final tabs = ['1', '2', '5'];
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget result;
    result = Scaffold(
      // appBar: AppBar(
      //   title: Text('cust'),
      // ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              title: Text('Nested'),
              pinned: true,
              floating: true,
              snap: true,
              expandedHeight: 200,
            ),
            // SliverToBoxAdapter(
            //   child: Center(
            //       child: Container(
            //     color: Colors.black,
            //     height: 100,
            //   )),
            // ),
            // SliverPersistentHeader(delegate: PreferredSize(child: Container(color: Colors.pink,height: 100,), preferredSize: 100))
          ];
        },
        body: NestedScrollView(
          headerSliverBuilder: (context, inner) {
            return [ 
            
              SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Container(
                    color: Colors.greenAccent,
                    height: 100,
                    width: 400,
                    child: Text('$index'),
                  );
                }, childCount: 50),
              ),
            ];
          },
          body: Column(
            children: <Widget>[
              TabBar(
                controller: tabController,
                tabs: tabs.map((v) => Tab(text: v)).toList(),
              ),
              Expanded(
                child: TabBarView(
                  controller: tabController,
                  children: tabs
                      .map((v) => Builder(builder: (context) {
                            return ListView.builder(
                              padding: EdgeInsets.only(top: 0),
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  color: Colors.red,
                                  child: Text('$index'),
                                  height: 50,
                                  width: 500,
                                );
                              },
                              itemCount: 120,
                            );
                          }))
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );

    return result;
  }
}
