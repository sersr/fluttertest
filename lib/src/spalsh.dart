import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart' show CupertinoActivityIndicator;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toast/toast.dart';
import '../bloc/myappbloc.dart';
import 'home.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin {
  AnimationController controller;

  final _tweenoffset = Tween<Offset>(begin: Offset(0.12, 0), end: Offset.zero);

  Animation<Offset> _position;
  bool isHomePage = false;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    _position = CurvedAnimation(parent: controller, curve: Curves.easeInOut).drive(_tweenoffset);
  }

  @override
  Widget build(BuildContext context) {
    final home = Scaffold(
      key: PageStorageKey<String>('scaffold'),
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text('Home Page'),
      ),
      body: HomePage(),
      floatingActionButton: FlatButton(
        color: Colors.blue,
        child: Text('go'),
        onPressed: () {
          // _neverSatisfied();
          // messages();
          Navigator.pushNamed(context, '/adpage');
        },
      ),
    );
    final result = Stack(
      children: <Widget>[
        SlideTransition(
          position: _position,
          child: home,
        ),
        if (!isHomePage)
          FirstPage(
            controller: controller,
            vcbk: goHome,
          )
      ],
    );

    return result;
  }

  void goHome() {
    setState(() {
      isHomePage = true;
    });
  }
}

class HomePage extends StatefulWidget {
  HomePage();
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  bool _wait;
  @override
  void initState() {
    super.initState();
    _wait = true;
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  final entry = OverlayEntry(
    builder: (context) => Positioned(
      bottom: 70,
      child: IgnorePointer(
        child: Material(
          color: Colors.transparent,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Color.fromARGB(140, 20, 20, 20)),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Container(
                child: Text(
                  '再按一次退出',
                  // softWrap: true,
                  style: TextStyle(fontSize: 16, color: Colors.white70),
                ),
                // width: 100,
              ),
            ),
          ),
        ),
      ),
    ),
  );

  @override
  Future<bool> didPopRoute() {
    if (_wait) {
      setState(() {
        _wait = false;
      });
      Overlay.of(context).insert(entry);
      Future.delayed(Duration(milliseconds: 1500), () {
        entry.remove();
        setState(() {
          _wait = true;
        });
      });
      return Future.value(true);
    }
    return super.didPopRoute();
  }

  @override
  Widget build(BuildContext context) {
    final blocManager = BlocProvider<MyAppBloc>(
      create: (context) => MyAppBloc(),
      child: BlocProvider<ItemsBloc>(
        create: (context) => ItemsBloc(),
        child: _ListPage(),
      ),
    );
    return blocManager;
  }
}

class FirstPage extends StatefulWidget {
  const FirstPage({Key key, @required this.controller, @required this.vcbk}) : super(key: key);
  final AnimationController controller;
  final VoidCallback vcbk;
  @override
  _FirstPageState createState() {
    return _FirstPageState();
  }
}

class _FirstPageState extends State<FirstPage> {
  int _remainSeconds;
  Timer timer;
  int alltime = 6;

  @override
  void initState() {
    super.initState();
    remainSeconds = alltime - 1;
    timer = Timer.periodic(Duration(seconds: 1), _decrease);
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void _decrease(Timer time) {
    if (time.tick == alltime)
      widget.controller.forward().whenComplete(() {
        widget.vcbk();
      });
    setState(() {
      remainSeconds--;
    });
  }

  int get remainSeconds => _remainSeconds;
  set remainSeconds(int value) {
    _remainSeconds = math.max(0, value);
  }

  @override
  Widget build(BuildContext context) {
    final position = CurvedAnimation(parent: widget.controller, curve: Curves.easeInOut).drive(
      Tween<Offset>(begin: Offset.zero, end: Offset(-1.0, 0.0)),
    );
    final stack = SlideTransition(
      position: position,
      child: Stack(children: [
        Container(
          color: Colors.red,
          child: SizedBox.expand(
            child: FlutterLogo(),
          ),
        ),
        Positioned(
            bottom: 10,
            right: 10,
            child: FlatButton(
              color: Colors.blue,
              child: Text('跳过 $remainSeconds'),
              onPressed: () {
                timer?.cancel();
                widget.controller.forward().whenComplete(() {
                  widget.vcbk();
                });
              },
            )),
      ]),
    );
    return stack;
  }
}

// 广告 。。。
class AdPage extends StatefulWidget {
  @override
  _AdPageState createState() => _AdPageState();
}

class _AdPageState extends State<AdPage> {
  int _remainSeconds;
  Timer timer;
  int alltime = 6;
  void _decrease(Timer time) {
    if (time.tick == alltime) {
      _wGoHome();
    }
    setState(() {
      remainSeconds--;
    });
  }

  int get remainSeconds => _remainSeconds;
  set remainSeconds(int value) {
    _remainSeconds = math.max(0, value);
  }

  @override
  void initState() {
    super.initState();
    remainSeconds = alltime - 1;
    timer = Timer.periodic(Duration(seconds: 1), _decrease);
  }

  @override
  Widget build(BuildContext context) {
    final Widget wecomPage = Stack(children: [
      Container(
        color: Colors.red,
        child: SizedBox.expand(
          child: FlutterLogo(),
        ),
      ),
      Positioned(
          bottom: 10,
          right: 10,
          child: FlatButton(
            color: Colors.blue,
            child: Text('跳过 $remainSeconds'),
            onPressed: _wGoHome,
          )),
    ]);
    return wecomPage;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void _wGoHome() {
    timer?.cancel();
    Navigator.maybePop(context);
  }
}

// List detail page
class _ListPage extends StatefulWidget {
  const _ListPage({Key key}) : super(key: key);
  createState() => _ListPageState();
}

class _ListPageState extends State<_ListPage> {
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  // List<Home> items = List<Home>.generate(12, (v)=>Home(key: ValueKey<int>(v),index: v,));
  // List<Home> itemCaches = [];
  ItemsBloc itemsbloc;
  @override
  Widget build(BuildContext context) {
    //ignore: close_sinks
    MyAppBloc bloc = BlocProvider.of<MyAppBloc>(context);
    itemsbloc = BlocProvider.of<ItemsBloc>(context);
    return NotificationListener(
      onNotification: (ScrollNotification notification) {
        if (notification is ScrollStartNotification) {
          bloc.add(ShowOrHide.scrolling);
        }
        return false;
      },
      child: BlocBuilder<ItemsBloc, List<int>>(
        condition: (pre, current) => true,
        builder: (context, items) => SmartRefresher(
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus mode) {
              Widget body;
              if (mode == LoadStatus.idle) {
                body = Text("上拉加载");
              } else if (mode == LoadStatus.loading) {
                body = CupertinoActivityIndicator();
              } else if (mode == LoadStatus.failed) {
                body = Text("加载失败！点击重试！");
              } else if (mode == LoadStatus.canLoading) {
                body = Text("松手,加载更多!");
              } else {
                body = Text("没有更多数据了!");
              }
              return Container(
                height: 55.0,
                child: Center(child: body),
              );
            },
          ),
          child: ListView.builder(
            key: ValueKey<int>(32445),
            // controller: controller,
            itemBuilder: (context, index) {
              return Container(color: Colors.cyan, child: Home(key: ValueKey<int>(index), index: items[index]));
            },
            itemCount: items.length,
          ),
          controller: _refreshController,
        ),
      ),
    );
  }

// void _data(){
//   final itemlength = items.length;
//     itemCaches = List<Home>.generate(10, (v)=>Home(index: v+itemlength,key: ValueKey<int>(v+itemlength),));
// }
  void _onLoading() async {
    // monitor network fetch
    // _data();
    // items.addAll(itemCaches);
    // // if failed,use loadFailed(),if no data return,use LoadNodata()
    // if (mounted) setState(() {});
    itemsbloc.add(ReEvent.loading);
    // await Future.delayed(duration);
    await Future.delayed(Duration(milliseconds: 1100));
    _refreshController.loadComplete();
  }

  void _onRefresh() async {
    // monitor network fetch
    // if(items.length>100)
    //   items = items.getRange(0, 14);
    // _data();
    // await Future.delayed(duration);
    // items.insertAll(0, itemCaches);
    // if (mounted) setState(() {});
    // if failed,use refreshFailed()
    itemsbloc.add(ReEvent.refresh);
    await Future.delayed(Duration(milliseconds: 1100));
    _refreshController.refreshCompleted();
  }
}
