import '../bloc/myappbloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef BoolCalb = Function({Mark mark});
typedef Mark = void Function();

final duration = Duration(milliseconds: 440);
// final _fastoutslowin = CurveTween(curve: Curves.fastOutSlowIn);
final _offsetTween = Tween<Offset>(begin: Offset.zero, end: Offset(-0.25, 0.0));
final _hoffsetTween = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.75, 0.0));

class Home extends StatelessWidget {
  const Home({Key key, this.index}) : super(key: key);
  final int index;

  @override
  Widget build(BuildContext context) {
    return _HomePage(
      index: index,
    );
  }
}

class _HomePage extends StatefulWidget {
  _HomePage({Key key, this.index}) : super(key: key);
  final int index;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<Offset> _fastPostion;
  Animation<Offset> _hfastPostion;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: duration);
    // _fastPostion = controller.drive(_fastoutslowin).drive(_offsetTween);
    // _hfastPostion = controller.drive(_hoffsetTween.chain(_fastoutslowin));
    // _fastPostion  = _offsetTween.animate(CurvedAnimation(parent: controller,curve: Curves.easeInOut));
    _fastPostion = CurvedAnimation(parent: controller,curve: Curves.easeInOut).drive(_offsetTween);
    _hfastPostion  = CurvedAnimation(parent: controller,curve: Curves.easeInOut).drive(_hoffsetTween);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _animationTo0() {
    if (controller.value != 0.0) controller.animateTo(0.0, duration: duration, curve: Curves.fastOutSlowIn);
  }

  void _animationTo1() {
    if (controller.value != 1.0) controller.animateTo(1.0, duration: duration, curve: Curves.fastOutSlowIn);
  }

  void _dragEnd() {
    if (controller.value < 0.5) {
      _animationTo0();
      // show = false;
    } else {
      _animationTo1();
      // show = true;
      bloc.add(ShowOrHide.no);
    }
  }

  bool show = false;
  MyAppBloc bloc;
  @override
  Widget build(BuildContext context) {
    //ignore: close_sinks
    bloc = BlocProvider.of<MyAppBloc>(context);

    final Widget del = SlideTransition(
      position: _hfastPostion,
      child: InkWell(
        onTap: () {
          _animationTo0();
        },
        child: Container(
          color: Colors.red,
          width: double.infinity,
          height: 50,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Center(
                      child: Text(
                    '删除',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                    softWrap: false,
                  ))),
              Expanded(
                flex: 3,
                child: SizedBox(),
              )
            ],
          ),
        ),
      ),
    );

    final Widget slider = SlideTransition(
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed('/second');
        },
        child: Container(
          height: 50,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 10,
              ),
              Text('这是 ${widget.index}'),
              Expanded(
                flex: 2,
                child: SizedBox(),
              ),
              Text('${widget.index}'),
              SizedBox(
                width: 10,
              )
            ],
          ),
        ),
      ),
      position: _fastPostion,
    );

    final Widget _stack = Stack(
      children: <Widget>[
        GestureDetector(
          key: ValueKey<int>(widget.index),
          onHorizontalDragStart: (DragStartDetails d) {
            bloc.add(ShowOrHide.scrolling);
          },
          onHorizontalDragUpdate: (DragUpdateDetails d) {
            controller.value -= d.delta.dx / 100;
          },
          onHorizontalDragEnd: (DragEndDetails d) {
            _dragEnd();
          },
          child: slider,
        ),
        del
      ],
    );

    final Widget result = BlocListener<MyAppBloc, bool>(
      listener: (pre, state) {
        if (state)
          //  && show)
          _animationTo0();
      },
      child: _stack,
    );

    return result;
  }
}

// class _MyClipper extends CustomClipper<Rect>{
//   @override
//   Rect getClip(Size size) {
//     return new Rect.fromLTRB(0,0, size.width,  size.height);
//   }

//   @override
//   bool shouldReclip(CustomClipper<Rect> oldClipper) {
//     return false;
//   }
// }
