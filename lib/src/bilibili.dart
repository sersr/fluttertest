import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../extended_nested_scroll_view.dart';
import 'sticky.dart';
// import 'package:flutter_sticky_header/flutter_sticky_header.dart';
// import 'package:sticky_headers/sticky_headers/widget.dart';

class BbHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _HomePage();
  }
}

class _HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<_HomePage> with TickerProviderStateMixin {
  int _currentIndex = 0;
  TabController tabController;
  final scrollController = ScrollController();
  final scaffoldkey = GlobalKey<ScaffoldState>();

  final bottomBars = ['首页', '频道', '动态', '我'];
  final tabs = ['直播', '推荐', '追番', '热门', '还有', '一些', '其他'];
  final tabViews = ['直播', '推荐', '追番', '热门', '还有', '一些', '其他'];
  final refreshKeys = List<GlobalKey<RefreshIndicatorState>>.generate(7, (_) => GlobalKey<RefreshIndicatorState>());

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    //
    //NestedScrollView and ScrollConfiguration
    final nestedScroll = ScrollConfiguration(
      behavior: MyScrollBehavior(),
      child: ExtendedNestedScrollView(
        controller: scrollController,
        physics: ScrollPhysics(),
        headerSliverBuilder: buildSliver,
        pinnedHeaderSliverHeightBuilder: pinned,
        body: TabBarView(controller: tabController, children: List.generate(tabs.length, _refreshCustomView)),
      ),
    );

    // Navigator Context.
    final navigatorContext = IndexedStack(
      index: _currentIndex,
      children: <Widget>[nestedScroll, ...List.generate(3, generBodys)],
    );

    //Navigator Bar
    final navigatorBar = BottomNavigationBar(
      selectedFontSize: 12.0,
      showUnselectedLabels: true,
      currentIndex: _currentIndex,
      backgroundColor: Colors.white,
      unselectedItemColor: Colors.black87,
      selectedItemColor: Colors.pink[400],
      type: BottomNavigationBarType.fixed,
      items: List<BottomNavigationBarItem>.generate(4, generBarItems),
      onTap: (int index) {
        if (_currentIndex == index && _currentIndex == 0) {
          double offset = scrollController.offset;
          Duration duration = Duration(milliseconds: (offset / (offset + 200) * 1000).toInt());
          scrollController.animateTo(0.0, curve: Curves.fastOutSlowIn, duration: duration);
          refreshKeys[tabController.index].currentState.show();
          return;
        }
        setState(() {
          _currentIndex = index;
        });
      },
    );

    final result = Scaffold(
      key: scaffoldkey,
      body: navigatorContext,
      bottomNavigationBar: navigatorBar,
      floatingActionButton: FlatButton(
        color: Colors.cyan,
        child: Text('Sticky'),
        onPressed: () {
          Navigator.pushNamed(context, "/sticky");
        },
      ),
    );

    return result;
  }

  //NestedScrollview headSliver
  List<Widget> buildSliver(BuildContext context, bool inner) {
    return [
      SliverAppBar(
        automaticallyImplyLeading: false,
        // snap: true,
        pinned: true,
        floating: true,
        backgroundColor: Color.fromRGBO(250, 250, 250, 1),
        title: Text('Bilibili Style', style: TextStyle(color: Colors.black)),
        leading: Padding(
          padding: const EdgeInsets.all(2.0),
          child: CircleAvatar(
            backgroundImage: AssetImage('assets/images/portrait.png'),
            radius: 15,
            maxRadius: 15,
            minRadius: 15,
          ),
        ),
        bottom: TabBar(
          controller: tabController,
          indicatorSize: TabBarIndicatorSize.label,
          indicatorPadding: EdgeInsets.only(bottom: 4),
          indicatorColor: Colors.pink[400],
          labelColor: Colors.pink[400],
          unselectedLabelColor: Colors.black87,
          isScrollable: true,
          tabs: tabs.map((tab) => Tab(text: tab)).toList(),
        ),
      ),
      // SliverFillRemaining(
      //   // hasScrollBody: false,
      //   child:
      // )
    ];
  }

  //CustomScrollView Body
  Widget _refreshCustomView(index) {
    return RefreshIndicator(
      displacement: 10,
      key: refreshKeys[index],
      onRefresh: onRefresh,
      child: CustomScrollView(
        key: PageStorageKey<int>(index),
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return Container(
                  child: Text('$index'),
                  color: index % 2 == 0 ? Colors.cyanAccent : Colors.green,
                  height: 100,
                );
              },
              childCount: 10,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 200,
              color: Colors.cyan,
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    scrollController?.dispose();
    tabController.dispose();
    super.dispose();
  }

  Future<void> onRefresh() {
    return Future<void>.delayed(Duration(seconds: 2), () {
      scaffoldkey.currentState?.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Container(
          child: Text('nihao'),
        ),
        backgroundColor: Color.fromRGBO(20, 20, 20, 0.5),
      ));
    });
  }

  double pinned() {
    return 48.0 + MediaQuery.of(context).padding.top;
  }

  Widget generBodys(int index) {
    return Center(
      child: Container(
        color: index % 2 == 0 ? Colors.blue : Colors.pink,
        height: 100,
        width: 200,
        child: Text(bottomBars[index + 1]),
      ),
    );
  }

  BottomNavigationBarItem generBarItems(int index) {
    return BottomNavigationBarItem(
      icon: Icon(index == 0 ? Icons.home : index == 1 ? Icons.bubble_chart : index == 2 ? Icons.data_usage : Icons.tv),
      title: Text(bottomBars[index]),
    );
  }
}
