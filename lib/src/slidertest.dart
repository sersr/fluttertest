import 'package:flutter/material.dart';

class SliderTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SliderTestState();
  }
}

class _SliderTestState extends State<SliderTest> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<Offset> _position;
  Animation<Offset> _secposition;
  Tween<Offset> _tweenOffset = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero);
  Tween<Offset> _sectweenOffset = Tween<Offset>(begin: Offset.zero, end: Offset(-0.2, 0.0));
  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    _position = CurvedAnimation(curve: Curves.easeInOut, parent: controller).drive(_tweenOffset);
    _secposition = CurvedAnimation(parent: controller, curve: Curves.easeInOut).drive(_sectweenOffset);
  }

  @override
  Widget build(BuildContext context) {
    final Widget sec = SlideTransition(
      position: _position,
      child: Container(
        color: Colors.blue,
        height: 200,
        width: 200,
      ),
    );
    final Widget result = SlideTransition(position: _secposition, child: sec);
    return Scaffold(
      body: result,
    );
  }
}
