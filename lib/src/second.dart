import 'package:flutter/material.dart';

class Second extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              FractionallySizedBox(
                widthFactor: 0.7,
                child: Container(
                  color: Colors.blueGrey,
                  height: 100,
                ),
              ),
              Container(
                color: Colors.red,
                height: 100,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/bilibili");
                },
                child: FractionalTranslation(
                  child: SizedOverflowBox(
                    child: Container(
                      height: 250,
                      width: 250,
                      color: Colors.brown,
                    ),
                    size: Size(200, 200),
                  ),
                  translation: Offset(0.5, 0.0),
                ),
              ),
              Container(
                color: Colors.red,
                height: 100,
              ),
              Container(
                color: Colors.blue,
                height: 100,
              ),
              Container(
                color: Colors.red,
                height: 100,
              ),
              Container(
                color: Colors.greenAccent,
                height: 100,
              ),
              Container(
                color: Colors.yellow,
                height: 100,
              ),
              Container(
                color: Colors.red,
                height: 100,
              ),
              Row(
                children: <Widget>[
                  Container(
                    color: Colors.amberAccent,
                    height: 100,
                    width: 100,
                  ),
                  Container(
                    color: Colors.red,
                    height: 100,
                    width: 100,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
