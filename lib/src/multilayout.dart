import 'package:flutter/material.dart';

class Mylayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      appBar: AppBar(
        title: Text('layout'),
      ),
      body: Mult(),
    ));
  }
}

class Mult extends StatefulWidget {
  @override
  _MultState createState() => _MultState();
}

class _MultState extends State<Mult> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.red,
        height: 600,
        width: 490,
        child: CustomMultiChildLayout(
          delegate: _TeMultilayout(),
          children: <Widget>[
            LayoutId(
              child: Container(
                height: 100,
                width: 200,
                child: Container(
                  color: Colors.cyan,
                ),
              ),
              id: 'ww',
            ),
            LayoutId(
              id: 'qq',
              child: Container(
                color: Colors.green,
                height: 200,
                width: 100,
              ),
            )
          ],
        ));
  }
}

class _TeMultilayout extends MultiChildLayoutDelegate {
  @override
  void performLayout(Size size) {
    var con = BoxConstraints(maxWidth: size.width, maxHeight: size.height);
    print(con);
    Size wwoffset = layoutChild('ww', con);
    positionChild('ww', Offset(0.0, 0.0));
    layoutChild('qq', con);
    positionChild('qq', Offset(0.0, wwoffset.height));
  }

  @override
  bool shouldRelayout(MultiChildLayoutDelegate oldDelegate) {
    return false;
  }
}
