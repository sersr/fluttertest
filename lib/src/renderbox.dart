import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MysingleWidget extends SingleChildRenderObjectWidget {
  MysingleWidget({this.child, this.offset}) : super(child: child);
  final Widget child;
  final Offset offset;
  @override
  MyRenderBox createRenderObject(BuildContext context) {
    return MyRenderBox(offset: offset);
  }

  @override
  void updateRenderObject(BuildContext context, MyRenderBox renderObject) {
    renderObject._offset = offset;
  }
}

class MyRenderBox extends RenderProxyBox {
  MyRenderBox({RenderBox child, Offset offset})
      : _offset = offset,
        super(child);

@override
  void setupParentData(RenderBox child) {
    if(child is! BoxParentData)
      child.parentData = BoxParentData();
    
  }
  Offset _offset;
  @override
  void performLayout() {
    final BoxParentData childParentData = child.parentData;
    print('nihaossssw');
    childParentData.offset = _offset;
    child.layout(constraints, parentUsesSize: true);
    size = child.size;
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    final BoxParentData childParentData = child.parentData;
    print('nihaossssw');
    print('nihao');
    context.paintChild(child, offset + childParentData.offset);
  }
}

class Mw extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MysingleWidget(
        offset: Offset(115, 220),
        child: Container(
          color: Colors.cyan,
          child: Text('nihaofwfw'),
        ),
      ),
    );
  }
}
