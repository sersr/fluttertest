import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';

class MyScrollBehavior extends ScrollBehavior {
  const MyScrollBehavior();

  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    // When modifying this function, consider modifying the implementation in
    // _MaterialScrollBehavior as well.
    switch (getPlatform(context)) {
      case TargetPlatform.iOS:
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return child;
    }
    return null;
  }
}

class Sticky extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StickyState();
  }
}

class _StickyState extends State<Sticky> with SingleTickerProviderStateMixin implements ScrollContext {
  ScrollController controller;
  ScrollPosition position;
  ScrollPhysics physics;
  ScrollController controller2;
  @override
  void initState() {
    super.initState();
    controller = ScrollController();
    tabController = TabController(length: tabs.length, vsync: this);
    controller2 = ScrollController();
    physics = ScrollConfiguration.of(context).getScrollPhysics(context);
    final ScrollPosition oldposition = position;
    position = controller.createScrollPosition(physics, this, oldposition);
    controller?.attach(position);
  }

  TabController tabController;
  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  final tabx = ['1', '2', '5'];
  final tabs = ['nm', 'fwf', 'disan'];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Text('Sticky Header'),
            bottom: TabBar(tabs: tabs.map((v) => Tab(child: Text(v))).toList())),
        body: TabBarView(
          children: <Widget>[
            ...tabs.map(
              (v) => NestedScrollView(
                headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                  return [
                    SliverStickyHeader(
                      key: PageStorageKey<int>(393),
                      header: new Container(
                        height: 60.0,
                        color: Colors.lightBlue,
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        alignment: Alignment.centerLeft,
                        child: new Text(
                          'Header #0',
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      sliver: new SliverList(
                        delegate: new SliverChildBuilderDelegate(
                          (context, i) => new ListTile(
                            leading: new CircleAvatar(
                              child: new Text('0'),
                            ),
                            title: new Text('List tile #$i'),
                          ),
                          childCount: 4,
                        ),
                      ),
                    ),
                    SliverStickyHeader(
                      key: PageStorageKey<int>(989),
                      header: new Container(
                        height: 60.0,
                        color: Colors.lightBlue,
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        alignment: Alignment.centerLeft,
                        child: new Text(
                          'Header #1',
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      sliver: new SliverList(
                        delegate: new SliverChildBuilderDelegate(
                          (context, i) => new ListTile(
                            leading: new CircleAvatar(
                              child: new Text('0'),
                            ),
                            title: new Text('List tile #$i'),
                          ),
                          childCount: 54,
                        ),
                      ),
                    ),
                  ];
                },
                body: NestedScrollView(
                  controller: controller2,
                  headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                    return [
                      SliverStickyHeader(
                        key: PageStorageKey<int>(33),
                        header: new Container(
                          height: 60.0,
                          color: Colors.lightBlue,
                          padding: EdgeInsets.symmetric(horizontal: 16.0),
                          alignment: Alignment.centerLeft,
                          child: new Text(
                            'Header #0',
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        sliver: new SliverList(
                          delegate: new SliverChildBuilderDelegate(
                            (context, i) => new ListTile(
                              leading: new CircleAvatar(
                                child: new Text('0'),
                              ),
                              title: new Text('List tile #$i'),
                            ),
                            childCount: 4,
                          ),
                        ),
                      ),
                      SliverStickyHeader(
                        key: PageStorageKey<int>(99),
                        header: new Container(
                          height: 60.0,
                          color: Colors.lightBlue,
                          padding: EdgeInsets.symmetric(horizontal: 16.0),
                          alignment: Alignment.centerLeft,
                          child: new Text(
                            'Header #1',
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        sliver: new SliverList(
                          delegate: new SliverChildBuilderDelegate(
                            (context, i) => new ListTile(
                              leading: new CircleAvatar(
                                child: new Text('0'),
                              ),
                              title: new Text('List tile #$i'),
                            ),
                            childCount: 54,
                          ),
                        ),
                      ),
                    ];
                  },
                  body: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            color: Colors.green,
                            child: TabBar(
                              controller: tabController,
                              labelColor: Colors.blue,
                              tabs: tabx.map((v) => Tab(text: v)).toList(),
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: tabController,
                          children: tabx
                              .map((v) => ListView.builder(
                                    itemBuilder: (BuildContext context, int index) {
                                      return Container(
                                        color: Colors.red,
                                        child: Text('$index'),
                                        height: 50,
                                        width: 500,
                                      );
                                    },
                                    itemCount: 120,
                                  ))
                              .toList(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  
  AxisDirection get axisDirection => AxisDirection.down;

  @override
  
  BuildContext get notificationContext => context;

  @override
  void setCanDrag(bool value) {
   
  }

  @override
  void setIgnorePointer(bool value) {
   
  }

  @override
  void setSemanticsActions(Set<SemanticsAction> actions) {
    
  }

  @override
  BuildContext get storageContext => context;

  @override
  TickerProvider get vsync => this;
}
