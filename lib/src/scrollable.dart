import 'package:demowin/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'dart:math' as math;

class Pinned extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      body: PinnedScroll(),
    ));
  }
}

class PinnedScroll extends StatefulWidget {
  @override
  _PinnedScrollState createState() => _PinnedScrollState();
}

class _PinnedScrollState extends State<PinnedScroll> {
  final ScrollController scrollController = ScrollController();
  final ScrollController mainScrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: ExtendedNestedScrollView(
        pinnedHeaderSliverHeightBuilder: () {
          return 50 + MediaQuery.of(context).padding.top;
        },
        controller: mainScrollController,
        headerSliverBuilder: (context, inner) {
          return <Widget>[
            // SliverPersistentHeader(
            //   pinned: true,
            //   floating: true,
            //     delegate: Mydela(TabBar(tabs: <Widget>[
            //     ],))),
            // SliverList(
            //     delegate: SliverChildBuilderDelegate((context, index) {
            //   return Container(
            //     height: 80,
            //     color: index % 2 == 0 ? Colors.grey : Colors.red,
            //   );
            // }, childCount: 30)),
            SliverAppBar(
              pinned: true,
              floating: true,
              title: Text('fwfw'),
              bottom: TabBar(
                tabs: <Widget>[
                  Tab(
                    text: 'fwfw',
                  ),
                  Tab(text: 'fwwwaaa')
                ],
              ),
            ),
            SliverFillRemaining(
                child: TabBarView(
              children: <Widget>[
                RefreshIndicator(
                                  child: NotificationListener(
                    onNotification: (ScrollNotification notification) {
                      if (notification is ScrollUpdateNotification) {
                        mainScrollController.position.setPixels(
                            (mainScrollController.offset + notification.scrollDelta).clamp(0, 50 + MediaQuery.of(context).padding.top));
                      }
                      return false;
                    },
                    child: CustomScrollView(
                      slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildBuilderDelegate((context, index) {
                          return Container(
                            height: 80,
                            color: index % 2 == 0 ? Colors.grey : Colors.red,
                          );
                        }, childCount: 30))
                      ],
                    ),
                  ), onRefresh: onRefresh,
                ),
                Container(
                  color: Colors.grey,
                  height: 100,
                  width: 200,
                ),
              ],
            )),
            // SliverStickyHeader(
            //   header: Container(
            //     height: 100,
            //     child: Text('nhafw'),
            //     color: Colors.grey,
            //   ),
            //   sliver: SliverList(
            //       delegate: SliverChildBuilderDelegate((context, index) {
            //     return Container(
            //       color: index % 2 == 0 ? Colors.pink : Colors.indigo,
            //       height: 120,
            //     );
            //   }, childCount: 20)),
            // )
          ];
        },
        body: null,
      ),
    );
  }
  Future<void> onRefresh() {
    return Future<void>.delayed(Duration(seconds: 2), () {
      Scaffold.of(context).showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Container(
          child: Text('nihao'),
        ),
        backgroundColor: Color.fromRGBO(20, 20, 20, 0.5),
      ));
    });
  }
  @override
  void dispose() {
    mainScrollController.dispose();
    scrollController.dispose();
    super.dispose();
  }
}

class Mydela extends SliverPersistentHeaderDelegate {
  Mydela(this.child);
  final Widget child;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    print(shrinkOffset);
    return SizedBox.expand(child: child);
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => 100;

  @override
  // TODO: implement minExtent
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}

class _StatusBarPaddingSliver extends SingleChildRenderObjectWidget {
  const _StatusBarPaddingSliver({
    Key key,
    @required this.maxHeight,
    this.scrollFactor = 5.0,
  })  : assert(maxHeight != null && maxHeight >= 0.0),
        assert(scrollFactor != null && scrollFactor >= 1.0),
        super(key: key);

  final double maxHeight;
  final double scrollFactor;

  @override
  _RenderStatusBarPaddingSliver createRenderObject(BuildContext context) {
    return _RenderStatusBarPaddingSliver(
      maxHeight: maxHeight,
      scrollFactor: scrollFactor,
    );
  }
}

class _RenderStatusBarPaddingSliver extends RenderSliver {
  _RenderStatusBarPaddingSliver({
    @required double maxHeight,
    @required double scrollFactor,
  })  : assert(maxHeight != null && maxHeight >= 0.0),
        assert(scrollFactor != null && scrollFactor >= 1.0),
        _maxHeight = maxHeight,
        _scrollFactor = scrollFactor;

  // The height of the status bar
  double get maxHeight => _maxHeight;
  double _maxHeight;
  set maxHeight(double value) {
    assert(maxHeight != null && maxHeight >= 0.0);
    if (_maxHeight == value) return;
    _maxHeight = value;
    markNeedsLayout();
  }

  // That rate at which this renderer's height shrinks when the scroll
  // offset changes.
  double get scrollFactor => _scrollFactor;
  double _scrollFactor;
  set scrollFactor(double value) {
    assert(scrollFactor != null && scrollFactor >= 1.0);
    if (_scrollFactor == value) return;
    _scrollFactor = value;
    markNeedsLayout();
  }

  @override
  void performLayout() {
    final double height = (maxHeight - constraints.scrollOffset / scrollFactor).clamp(0.0, maxHeight);
    geometry = SliverGeometry(
      paintExtent: math.min(height, constraints.remainingPaintExtent),
      scrollExtent: maxHeight,
      maxPaintExtent: maxHeight,
    );
  }
}
