import 'package:flutter/material.dart';

class PageBuilderTest extends StatefulWidget {
  createState() => _PageBuilderTestState();
}

class _PageBuilderTestState extends State<PageBuilderTest> with SingleTickerProviderStateMixin {
  // AnimationController controller;

  @override
  void initState() {
    super.initState();
    // controller = AnimationController(vsync: this, duration: Duration(milliseconds: 400));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: Center(
        child: Text('body'),
      ),
      floatingActionButton: FlatButton(
        color: Colors.brown,
        child: Text('return'),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return Scaffold(
              appBar: AppBar(title: Text('PageBuilder Page')),
              body: Center(
                child: Text('PageBuilder body'),
              ),
              floatingActionButton: FlatButton(
                color: Colors.cyan,
                child: Text('return'),
                onPressed: () {
                  Navigator.maybePop(context);
                },
              ),
            );
          }));
        },
      ),
    );
  }
}

// 自定义页面过度
class MyPageTransition extends PageTransitionsBuilder {
  final _hoffsetTween = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero);
    final _seoffsetTween = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(-0.2,0.0));
  @override
  Widget buildTransitions<T>(PageRoute<T> route, BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
        final sec = SlideTransition(
      child: child,
      position: CurvedAnimation(
        parent: secondaryAnimation,
        curve: Curves.easeInOut,
      ).drive(_seoffsetTween),
    );
    final slidetransition = SlideTransition(
      child: sec,
      position: CurvedAnimation(
        parent: animation,
        curve: Curves.easeInOut,
      ).drive(_hoffsetTween),
    );
    return slidetransition;
  }
}
