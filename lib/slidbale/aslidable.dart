import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class Myslidable extends StatelessWidget {
  Widget build(context) {
    void _showSnackBar(String string) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(string),
      ));
    }

    return Scaffold(
      appBar: AppBar(title: Text('Slidable'),),
      body: ListView.builder(
        itemCount: 50,
          itemBuilder:(context,index)=> Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: Container(
            color: Colors.white,
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.indigoAccent,
                child: Text('$index'),
                foregroundColor: Colors.white,
              ),
              title: Text('Tile n$index°'),
              subtitle: Text('SlidableDrawerDelegate'),
            ),
          ),
          actions: <Widget>[
            IconSlideAction(
              caption: 'Archive',
              color: Colors.blue,
              icon: Icons.archive,
              onTap: () => _showSnackBar('Archive'),
            ),
            IconSlideAction(
              caption: 'Share',
              color: Colors.indigo,
              icon: Icons.share,
              onTap: () => _showSnackBar('Share'),
            ),
          ],
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'More',
              color: Colors.black45,
              icon: Icons.more_horiz,
              onTap: () => _showSnackBar('More'),
            ),
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => _showSnackBar('Delete'),
            ),
          ],
        ),
      ),
    );
  }
}
